package com.aplicativo.sole.brinquedotroca;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

public class ActivityVisualizarBrinquedo extends AppCompatActivity implements AdapterView.OnItemClickListener {

    public static String icon = null;
    public AdapterListViewVisualizarBrinquedo adapterListView = null;
    private ArrayList<ParseObject> itens = null;
    private static final String[] vetDescricao = new String[]{" ", "boneca ", "carro", "jogo", "acessório", "boneco", "eletrônico", "pelúcia", "livro", "outros"};
    String des = null;
    final Context context = this;
    public static String nomeluserlogado = null;
    String tratamentousuario = null;
    String tratamentodescricao = null;
    String objetoId = null;
    String icone = "";
    int positionRemove = -1;
    ProgressDialog pdialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_visualizarbrinquedo);

        final Spinner descricao = (Spinner) findViewById(R.id.spn_descricao);

        final ImageButton meusbrinquedos = (ImageButton) findViewById(R.id.imb_meusbrinquedos);
        final ImageButton outrosbrinquedos = (ImageButton) findViewById(R.id.imb_outrosbrinquedos);
        final ImageButton lixeira = (ImageButton) findViewById(R.id.imb_lixeira);

        final TextView strlixeira = (TextView) findViewById(R.id.tv_lixeira);
        final TextView strdescricao = (TextView) findViewById(R.id.tv_brinquedo);

        final ListView listView = (ListView) findViewById(R.id.list_brinquedo);
        listView.setOnItemClickListener(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ArrayAdapter adp = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, vetDescricao);
        adp.setDropDownViewResource(android.R.layout.simple_spinner_item);

        meusbrinquedos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                pdialog = ProgressDialog.show(context, " Meus Brinquedos", " Por favor, aguarde ...", true);
                ParseQuery<ParseObject> query = null;
                query = new ParseQuery<ParseObject>("Brinquedo");
                query.whereEqualTo("usuario", nomeluserlogado);
                query.findInBackground(new FindCallback<ParseObject>() {
                    public void done(List<ParseObject> pobjs, ParseException e) {
                        if (e == null) {
                            pdialog.dismiss();
                            itens = (ArrayList) pobjs;
                            adapterListView = new AdapterListViewVisualizarBrinquedo(getApplicationContext(), itens);
                            listView.setAdapter(adapterListView);
                            listView.setCacheColorHint(Color.TRANSPARENT);
                            if (adapterListView.isEmpty()) {
                                pdialog.dismiss();
                                Toast.makeText(getApplicationContext(), " Ops !! Você não enviou brinquedos ainda !", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            pdialog.dismiss();
                            alertDisplayer();
                            // Toast.makeText(getApplicationContext(), "Desculpe, falha ao se conectar no servidor!", Toast.LENGTH_LONG).show();
                            finish();
                        }
                        pdialog.dismiss();
                    }
                });
                descricao.setVisibility(View.INVISIBLE);
                strdescricao.setVisibility(View.INVISIBLE);
                icone = "meusbrinquedos";
            }
        });


        descricao.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View arg1,
                                       int pos, long arg3) {
                des = parent.getSelectedItem().toString();
                tratamentousuario = des;
                tratamentodescricao = des;
                icone = "outrosbrinquedos";
                pdialog = ProgressDialog.show(context, " Outros Brinquedos ", " Por favor, aguarde ...", true);
                if (descricao.getItemAtPosition(pos).toString() == " ") {
                    pdialog.dismiss();
                } else {

                    ParseQuery<ParseObject> query = null;
                    query = new ParseQuery<ParseObject>("Brinquedo");
                    query.whereEqualTo("descricao", des);
                    query.whereNotEqualTo("usuario", nomeluserlogado);
                    query.findInBackground(new FindCallback<ParseObject>() {
                        public void done(List<ParseObject> pobjs, ParseException e) {
                            if (e == null) {
                                pdialog.dismiss();
                                itens = (ArrayList) pobjs;
                                adapterListView = new AdapterListViewVisualizarBrinquedo(getApplicationContext(), itens);
                                listView.setAdapter(adapterListView);
                                listView.setCacheColorHint(Color.TRANSPARENT);
                                if (adapterListView.isEmpty())
                                    Toast.makeText(getApplicationContext(), " Brinquedo não disponível !!", Toast.LENGTH_LONG).show();
                            } else {
                                pdialog.dismiss();
                                alertDisplayer();
                                finish();
                            }

                        }
                    });
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                des = "X";
            }
        });

        lixeira.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                pdialog = ProgressDialog.show(context, " Apagar Brinquedo ", " Por favor, aguarde ...", true);

                if (nomeluserlogado.equals(tratamentousuario)) {
                    ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("Brinquedo");
                    query.whereEqualTo("objectId", objetoId);
                    query.getFirstInBackground(new GetCallback<ParseObject>() {
                        @Override
                        public void done(ParseObject status, ParseException e) {
                            adapterListView.remove(positionRemove);
                            try {
                                pdialog.dismiss();
                                status.delete();
                                status.saveInBackground();

                            } catch (ParseException ex) {
                                ex.printStackTrace();
                                pdialog.dismiss();
                            }
                        }
                    });

                } else {
                    pdialog.dismiss();
                    //      Toast.makeText(getApplicationContext(), " Ops ! Você não selecionou um brinquedo ! ", Toast.LENGTH_LONG).show();
                }
                lixeira.setVisibility(View.INVISIBLE);
                strlixeira.setVisibility(View.INVISIBLE);
            }
        });

        outrosbrinquedos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                descricao.setVisibility(View.VISIBLE);
                strdescricao.setVisibility(View.VISIBLE);
                descricao.setAdapter(adp);
                lixeira.setVisibility(View.INVISIBLE);
                strlixeira.setVisibility(View.INVISIBLE);
                listView.setAdapter(null);
            }
        });

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

        ParseObject item = (ParseObject) adapterListView.getItem(position);
        objetoId = item.getObjectId();
        tratamentousuario = item.get("usuario").toString();

        // Toast.makeText(this, itemlist_visualizarbrinquedo.getObjectId() + " - " + itemlist_visualizarbrinquedo.get("descricao")+ itemlist_visualizarbrinquedo.get("usuario"),+ itemlist_visualizarbrinquedo.get("image") Toast.LENGTH_LONG).show();
        if (icone.equals("meusbrinquedos") || des.equals(" ")) {
            //  Toast.makeText(getApplicationContext(), "Meu brinquedo " + itemlist_visualizarbrinquedo.get("descricao"), Toast.LENGTH_LONG).show();
            positionRemove = position;
            alertDisplayerApagarBrinquedo();
        } else {

            //  alertDisplayerVisualizaroutros();
            ActivitySugestaoTroca.remetentenome = nomeluserlogado;
            // ActivitySugestaoTroca.flagTrocarMensagem = false;
            ActivitySugestaoTroca.destinatarionome = itens.get(position).get("usuario").toString();
            ActivitySugestaoTroca.destinatariodescricao = itens.get(position).get("descricao").toString();
            ActivitySugestaoTroca.destinatariobrinquedo = itens.get(position).getParseFile("image");
            Intent intent = new Intent(context, ActivitySugestaoTroca.class);
            startActivity(intent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_visualizarbrinquedo, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.mensagem) {

            Intent intent = new Intent(context, ActivityVisualizarMensagem.class);
            startActivity(intent);

        } else if (id == R.id.home) {

            Intent intent = new Intent(context, ActivityHome.class);
            startActivity(intent);


        } else if (id == R.id.carregar) {

            Intent intent = new Intent(context, ActivityCarregarBrinquedo.class);
            startActivity(intent);


        } else if (id == R.id.sair) {

            Intent intent = new Intent(context, ActivityLogout.class);
            startActivity(intent);
        } else {

            Intent intent = new Intent(context, ActivityEmail.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    private void alertDisplayer() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityVisualizarBrinquedo.this)
                .setTitle(" Problemas com o servidor ")
                .setMessage(" Desculpe ... Por favor logue novamente ")
                //       .setIcon(R.drawable.ic_launcher)
                .setNegativeButton(" NÃO ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Intent intent = new Intent(ActivityVisualizarBrinquedo.this, ActivityLogout.class);
                        startActivity(intent);
                        ParseUser.logOut();

                    }
                })
                .setPositiveButton(" OK ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Intent intent = new Intent(ActivityVisualizarBrinquedo.this, ActivityLogin.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                });
        builder.create().show();
    }

    private void alertDisplayerApagarBrinquedo() {

        final ImageButton lixeira = (ImageButton) findViewById(R.id.imb_lixeira);
        final TextView strlixeira = (TextView) findViewById(R.id.tv_lixeira);
        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityVisualizarBrinquedo.this)
                .setMessage("Apagar o brinquedo selecionado ?")
                .setNegativeButton(" NÃO ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .setPositiveButton(" SIM ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        lixeira.setVisibility(View.VISIBLE);
                        strlixeira.setVisibility(View.VISIBLE);
                    }
                });
        builder.create().show();
    }

}






