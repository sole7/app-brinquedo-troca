package com.aplicativo.sole.brinquedotroca;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageButton;

import com.parse.ParseFacebookUtils;


public class ActivityHome extends AppCompatActivity {

    final Context context = this;
    private static final String PREF_NAME = "LoginActivityPreferences";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_home);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.mensagem) {

            Intent intent = new Intent(context, ActivityVisualizarMensagem.class);
            startActivity(intent);


        } else if (id == R.id.carregar) {

            Intent intent = new Intent(context, ActivityCarregarBrinquedo.class);
            startActivity(intent);


        } else if (id == R.id.visualizar) {

            Intent intent = new Intent(context, ActivityVisualizarBrinquedo.class);
            startActivity(intent);


        } else if (id == R.id.sair) {

            Intent intent = new Intent(context, ActivityLogout.class);
            startActivity(intent);


        } else {
            Intent intent = new Intent(context, ActivityEmail.class);
            startActivity(intent);

        }

        return super.onOptionsItemSelected(item);

    }

}





