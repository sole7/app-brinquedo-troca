package com.aplicativo.sole.brinquedotroca;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.io.ByteArrayOutputStream;

public class ActivityCarregarBrinquedo extends AppCompatActivity {

    public static final int IMAGEM_INTERNA = 12;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final int MY_REQUEST__CODE = 1;
    public static String icon = null;
    public static String user = null;
    private Uri uri;
    private static final String[] vetDescricao = new String[]{" ", "boneca ", "carro", "jogo", "acessório", "boneco", "eletrônico", "pelúcia", "livro", "outros"};
    ImageView iv;
    String des;
    public static String emaildonobrinquedo;
    ImageButton camera;
    ImageButton diretorio;
    final Context context = this;
    ProgressDialog pdialog = null;
    Boolean ivnull = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_carregarbrinquedo);

        camera = (ImageButton) findViewById(R.id.imb_camera);
        diretorio = (ImageButton) findViewById(R.id.imb_diretorio);
        final ImageButton upload = (ImageButton) findViewById(R.id.imgupload);

        final Spinner descricao = (Spinner) findViewById(R.id.spndescricao);

        final ArrayAdapter adp = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, vetDescricao);
        adp.setDropDownViewResource(android.R.layout.simple_spinner_item);
        descricao.setAdapter(adp);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        descricao.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View arg1,
                                       int pos, long arg3) {
                des = parent.getSelectedItem().toString();
                upload.setVisibility(View.VISIBLE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                des = " ";
            }
        });


        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                icon = "camera";
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                }
            }
        });


        diretorio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                icon = "pasta";
                if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.READ_EXTERNAL_STORAGE) ==
                        PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(intent, IMAGEM_INTERNA);
                } else {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, IMAGEM_INTERNA);
                }
            }
        });

        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                if (des.equals(" ")) {

                    Toast.makeText(getApplicationContext(), " Ops! Voce não selecionou uma descrição!", Toast.LENGTH_LONG).show();

                } else if (iv == null || ivnull == true) {

                    Toast.makeText(getApplicationContext(), " Ops! Você não selecionou um brinquedo!", Toast.LENGTH_LONG).show();
                    ivnull = false;

                } else {

                    pdialog = ProgressDialog.show(context, " Enviar Brinquedo", "Por favor, aguarde ...", true);

                    Drawable drawable = iv.getDrawable();
                    Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                    byte[] data = stream.toByteArray();
                    ParseFile imageFile = new ParseFile("img.jpg", data);

                    ParseObject po = new ParseObject("Brinquedo");
                    po.put("descricao", des);
                    po.put("image", imageFile);
                    po.put("usuario", user);
                    po.put("contato", emaildonobrinquedo);
                    po.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {

                            pdialog.dismiss();

                            Snackbar.make(view, "Brinquedo enviado!", Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                            // Log.i("ENVIO_MSG", "TESTE");
                            Bitmap bitmap = BitmapFactory.decodeFile(null);
                            iv = (ImageView) findViewById(R.id.iv_brinquedodestinatario);
                            iv.setImageBitmap(bitmap);
                            ivnull = true;

                            alertDisplayerEnviarBrinquedo();


                        }
                    });
                }

            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        //    ParseFacebookUtils.onActivityResult(requestCode, resultCode, intent);
        super.onActivityResult(requestCode, resultCode, intent);
        if (icon.equals("camera")) {
            ivnull = false;
            if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {

                Bundle extras = intent.getExtras();
                Bitmap imageBitmap = (Bitmap) extras.get("data");
                iv = (ImageView) findViewById(R.id.iv_brinquedodestinatario);
                iv.setImageBitmap(imageBitmap);
            }

        } else {
            if (requestCode == IMAGEM_INTERNA) {
                if (resultCode == RESULT_OK) {
                    Uri imagemSelecionada = intent.getData();
                    String[] colunas = {MediaStore.Images.Media.DATA};
                    assert imagemSelecionada != null;
                    Cursor cursor = getContentResolver().query(imagemSelecionada, colunas, null, null, null);
                    assert cursor != null;
                    cursor.moveToFirst();
                    int indexColuna = cursor.getColumnIndex(colunas[0]);
                    String pathImg = cursor.getString(indexColuna);
                    cursor.close();
                    Bitmap bitmap = BitmapFactory.decodeFile(pathImg);
                    Bitmap bitmapReduzido = Bitmap.createScaledBitmap(bitmap, 1080, 1000, true);  /*solução pra rodar na api > que 23*/
                    iv = (ImageView) findViewById(R.id.iv_brinquedodestinatario);
                    iv.setImageBitmap(bitmapReduzido);

                }
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_carregarbrinquedo, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.mensagem) {

            Intent intent = new Intent(context, ActivityVisualizarMensagem.class);
            startActivity(intent);

        } else if (id == R.id.home) {

            Intent intent = new Intent(context, ActivityHome.class);
            startActivity(intent);


        } else if (id == R.id.visualizar) {

            Intent intent = new Intent(context, ActivityVisualizarBrinquedo.class);
            startActivity(intent);

        } else if (id == R.id.sair) {

            Intent intent = new Intent(context, ActivityLogout.class);
            startActivity(intent);
        } else {

            Intent intent = new Intent(context, ActivityEmail.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);

    }

    private void alertDisplayer() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityCarregarBrinquedo.this)
                .setTitle("Problemas com o servidor ")
                .setMessage("Desculpe ... Redirecionando você para o login ")
                //   .setIcon(R.drawable.ic_launcher)
                .setNegativeButton("NÂO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Intent intent = new Intent(ActivityCarregarBrinquedo.this, ActivityLogout.class);
                        startActivity(intent);
                        ParseUser.logOut();

                    }
                })
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Intent intent = new Intent(ActivityCarregarBrinquedo.this, ActivityLogin.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                });
        builder.create().show();
    }

    private void alertDisplayerEnviarBrinquedo() {

        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityCarregarBrinquedo.this)
                .setMessage("Continuar enviando brinquedo?")
                .setNegativeButton(" NÃO ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Intent intent = new Intent(context, ActivityHome.class);
                        startActivity(intent);
                    }
                })
                .setPositiveButton(" SIM ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        limpar();
                    }
                });
        builder.create().show();
    }

    public void limpar() {

        final ArrayAdapter adp = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, vetDescricao);
        adp.getItem(0);
        final Spinner descricao = (Spinner) findViewById(R.id.spndescricao);
        descricao.setAdapter(adp);
    }


}








