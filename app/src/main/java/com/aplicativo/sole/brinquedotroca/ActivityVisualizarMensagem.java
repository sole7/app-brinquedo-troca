package com.aplicativo.sole.brinquedotroca;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

public class ActivityVisualizarMensagem extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private ListView listView = null;
    public AdapterListViewVisulalizarMensagem adapterListView = null;
    private ArrayList<ParseObject> itens = null;
    private ArrayList<ParseObject> item = null;
    String des = null;
    final Context context = this;
    public static String nomeluserlogado = null;
    String objetoId = null;
    public ImageButton lixeira;
    public TextView strlixeira;
    public String icone = null;
    int positionRemove = -1;
    ProgressDialog pdialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_visualizarmensagens);
        final ImageButton recebidas = (ImageButton) findViewById(R.id.imb_recebidas);
        final ImageButton enviadas = (ImageButton) findViewById(R.id.imb_enviadas);

        listView = (ListView) findViewById(R.id.list_mensagens);
        listView.setOnItemClickListener(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        recebidas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                pdialog = ProgressDialog.show(context, " Mensagens Recebidas", " Por favor, aguarde ...", true);
                icone = "recebida";
                ParseQuery<ParseObject> query = null;
                query = new ParseQuery<ParseObject>("SugestaoTroca");
                query.whereEqualTo("destinatario", nomeluserlogado);
                query.orderByDescending("createdAt");
                query.findInBackground(new FindCallback<ParseObject>() {
                    public void done(List<ParseObject> pobjs, ParseException e) {
                        if (e == null) {
                            pdialog.dismiss();
                            itens = (ArrayList) pobjs;
                            adapterListView = new AdapterListViewVisulalizarMensagem(getApplicationContext(), itens);
                            listView.setAdapter(adapterListView);
                            listView.setCacheColorHint(Color.TRANSPARENT);
                            //   if()  Toast.makeText(getApplicationContext(), "Brinquedo não disponível !!", Toast.LENGTH_LONG).show();
                            if (adapterListView.isEmpty()) {
                                Toast.makeText(getApplicationContext(), " Caixa de mensagens recebidas vazia !", Toast.LENGTH_LONG).show();
                                pdialog.dismiss();
                            }
                        } else {
                            pdialog.dismiss();
                            alertDisplayer();
                            //  Toast.makeText(getApplicationContext(), "Desculpe, falha ao se conectar no servidor!", Toast.LENGTH_LONG).show();
                            finish();
                        }
                        //    if(adapterListView.isEmpty())  Toast.makeText(getApplicationContext(), " Brinquedo não disponível !!", Toast.LENGTH_LONG).show();;
                    }
                });

            }
        });

        enviadas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                pdialog = ProgressDialog.show(context, "Mensagens Enviadas", "Por favor, aguarde ...", true);
                icone = "enviadas";
                ParseQuery<ParseObject> query = null;
                query = new ParseQuery<ParseObject>("SugestaoTroca");
                query.whereEqualTo("remetente", nomeluserlogado);
                query.orderByDescending("createdAt");
                query.findInBackground(new FindCallback<ParseObject>() {
                    public void done(List<ParseObject> pobjs, ParseException e) {
                        if (e == null) {
                            pdialog.dismiss();
                            itens = (ArrayList) pobjs;
                            adapterListView = new AdapterListViewVisulalizarMensagem(getApplicationContext(), itens);
                            listView.setAdapter(adapterListView);
                            listView.setCacheColorHint(Color.TRANSPARENT);
                            if (adapterListView.isEmpty()) {
                                Toast.makeText(getApplicationContext(), " Caixa de mensagens enviadas vazia !", Toast.LENGTH_LONG).show();
                                pdialog.dismiss();
                            }
                        } else {
                            pdialog.dismiss();
                            alertDisplayer();
                            // Toast.makeText(getApplicationContext(), "Desculpe, falha ao se conectar no servidor!!", Toast.LENGTH_LONG).show();
                            finish();
                        }

                    }
                });
            }
        });

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

        ParseObject item = (ParseObject) adapterListView.getItem(position);
        objetoId = item.getObjectId();

        if (icone.equals("enviadas")) {

            ActivityAceitarTroca.nãosounotificacaoAceitarTroca = true;
            Intent intent = new Intent(context, ActivityAceitarTroca.class);
            startActivity(intent);

            ActivityAceitarTroca.destinatarionome = itens.get(position).get("remetente").toString();
            ActivityAceitarTroca.remetentenome = itens.get(position).get("destinatario").toString();
            ActivityAceitarTroca.destinatariobrinquedo = itens.get(position).getParseFile("fotoremetente");
            ActivityAceitarTroca.remetentebrinquedo = itens.get(position).getParseFile("foto");
            ActivityAceitarTroca.mensagem = itens.get(position).get("type").toString();
            ActivityAceitarTroca.enviada = true;
            ActivityAceitarTroca.nãosounotificacaoAceitarTroca = true;

        } else {

            ActivityAceitarTroca.nãosounotificacaoAceitarTroca = true;
            Intent intent = new Intent(context, ActivityAceitarTroca.class);
            startActivity(intent);

            ActivityAceitarTroca.destinatarionome = itens.get(position).get("destinatario").toString();
            ActivityAceitarTroca.remetentenome = itens.get(position).get("remetente").toString();
            ActivityAceitarTroca.destinatariobrinquedo = itens.get(position).getParseFile("foto");
            ActivityAceitarTroca.remetentebrinquedo = itens.get(position).getParseFile("fotoremetente");
            ActivityAceitarTroca.mensagem = itens.get(position).get("type").toString();
            ActivityAceitarTroca.enviada = false;

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_mensagem, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.visualizar) {

            Intent intent = new Intent(context, ActivityVisualizarBrinquedo.class);
            startActivity(intent);

        } else if (id == R.id.home) {

            Intent intent = new Intent(context, ActivityHome.class);
            startActivity(intent);


        } else if (id == R.id.carregar) {

            Intent intent = new Intent(context, ActivityCarregarBrinquedo.class);
            startActivity(intent);


        } else if (id == R.id.sair) {

            Intent intent = new Intent(context, ActivityLogout.class);
            startActivity(intent);


        } else {

            Intent intent = new Intent(context, ActivityEmail.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    private void alertDisplayer() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityVisualizarMensagem.this)
                .setTitle(" Problemas com o servidor ")
                .setMessage(" Desculpe ... Redirecionando você para o login ")
                //   .setIcon(R.drawable.ic_launcher)
                .setNegativeButton(" NÃO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Intent intent = new Intent(ActivityVisualizarMensagem.this, ActivityLogout.class);
                        startActivity(intent);
                        ParseUser.logOut();

                    }
                })
                .setPositiveButton(" OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Intent intent = new Intent(ActivityVisualizarMensagem.this, ActivityLogin.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                });
        builder.create().show();
    }

}






