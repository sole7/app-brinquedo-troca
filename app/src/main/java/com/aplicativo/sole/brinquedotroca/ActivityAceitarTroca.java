package com.aplicativo.sole.brinquedotroca;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseFile;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SendCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

public class ActivityAceitarTroca extends AppCompatActivity {

    public static String destinatarionome = null;
    public static boolean enviada = true;
    public static boolean nãosounotificacaoAceitarTroca = false;
    public static ParseFile destinatariobrinquedo = null;
    public static String remetentenome = null;
    public static ParseFile remetentebrinquedo = null;
    public static String mensagem = null;
    public static String nomeluserlogado = null;
    public static String objectIdAceitarTroca = null;
    ImageButton trocarsugestao;
    ImageButton agoranao;
    ImageButton aceito;
    final Context context = this;
    ProgressDialog pdialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_aceitartroca);

        final ImageView briquedodestinatario = (ImageView) findViewById(R.id.iv_brinquedodestinatario);
        final ImageView briquedoremetente = (ImageView) findViewById(R.id.iv_brinquedoremetente);

        final TextView destinatario = (TextView) findViewById(R.id.edt_destinatario);
        final TextView remetente = (TextView) findViewById(R.id.edt_remetente);
        final TextView edt_mensagem = (TextView) findViewById(R.id.edt_mensagem);
        final TextView aceitar = (TextView) findViewById(R.id.edt_aceitar);

        aceito = (ImageButton) findViewById(R.id.imb_aceitar);
        trocarsugestao = (ImageButton) findViewById(R.id.imb_sugeriroutro);
        agoranao = (ImageButton) findViewById(R.id.imb_agoranao);

        destinatario.setText(destinatarionome);
        remetente.setText(remetentenome);
        edt_mensagem.setText(mensagem);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActivityAceitarTroca.this, ActivityVisualizarMensagem.class);
                startActivity(intent);
            }
        });


        Parse.initialize(new Parse.Configuration.Builder(this)
                .applicationId("QTb0m9pdYtu3PAYrbN1jJ9tR9YHd8CDcHcTZl7Po")
                .clientKey("NUd1vsTAVmcpjU5ypsb9caEPCrG4kouzNYhgF0oq")
                .server("https://parseapi.back4app.com/").build());

        Parse.initialize(this);


        if (!enviada) {
            aceito.setVisibility(View.VISIBLE);
            aceitar.setVisibility(View.VISIBLE);
        } else {
            aceito.setVisibility(View.INVISIBLE);
            aceitar.setVisibility(View.INVISIBLE);
        }

        if (destinatariobrinquedo != null) {
            destinatariobrinquedo.getDataInBackground(new GetDataCallback() {
                public void done(byte[] data, ParseException e) {
                    if (e == null) {
                        Bitmap bmp = BitmapFactory.decodeByteArray(data, 0, data.length);
                        briquedodestinatario.setImageBitmap(bmp);
                    }
                }
            });
        }

        if (remetentebrinquedo != null) {
            remetentebrinquedo.getDataInBackground(new GetDataCallback() {
                public void done(byte[] data, ParseException e) {
                    if (e == null) {
                        Bitmap bmp2 = BitmapFactory.decodeByteArray(data, 0, data.length);
                        briquedoremetente.setImageBitmap(bmp2);
                    } else {
                        //  Log.d("BUZZ", "BUZZ");
                    }
                }
            });
        }

        trocarsugestao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(ActivityAceitarTroca.this, ActivitySugestaoTroca.class);
                ActivitySugestaoTroca.remetentenome = destinatarionome;
                ActivitySugestaoTroca.destinatarionome = remetentenome;
                ActivitySugestaoTroca.destinatariobrinquedo = remetentebrinquedo;
                ActivitySugestaoTroca.remetentebrinquedo = destinatariobrinquedo;
                startActivity(intent);

            }
        });

        agoranao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(ActivityAceitarTroca.this, ActivityHome.class);
                startActivity(intent);
            }
        });

        aceito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                final ParseObject poke = new ParseObject("AceitarTroca");

                poke.put("destinatario", remetentenome);
                poke.put("remetente", destinatarionome);
                poke.put("foto", remetentebrinquedo);
                poke.put("fotoremetente", destinatariobrinquedo);
                poke.put("mensagem", edt_mensagem.getText().toString());
                poke.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        objectIdAceitarTroca = poke.getObjectId();
                        sendParse();
                        Snackbar.make(view, " Que legal !! ", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                        Log.i("ENVIO_MSG", "TESTE");

                        alertDisplayeReceberNovasMensagens();
                    }
                });
            }
        });

        if (!nãosounotificacaoAceitarTroca) {

            Intent it = getIntent();
            JSONObject jobj = null;
            try {

                pdialog = ProgressDialog.show(context, " Notificação ", " Por favor, aguarde ...", true);

                jobj = new JSONObject(it.getExtras().getString("com.parse.Data"));
                JSONObject objTroca = new JSONObject(jobj.get("alert").toString());
                final String id = objTroca.get("id").toString();

                final ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("SugestaoTroca");
                query.whereEqualTo("objectId", id);
                query.getFirstInBackground(new GetCallback<ParseObject>() {
                    @Override
                    public void done(ParseObject status, ParseException e) {
                        if (e == null) {
                            pdialog.dismiss();
                            destinatarionome = status.get("destinatario").toString();
                            remetentenome = status.get("remetente").toString();
                            mensagem = status.get("type").toString();
                            destinatariobrinquedo = status.getParseFile("foto");
                            remetentebrinquedo = status.getParseFile("fotoremetente");

                            if (destinatariobrinquedo != null) {
                                destinatariobrinquedo.getDataInBackground(new GetDataCallback() {
                                    public void done(byte[] data, ParseException e) {
                                        if (e == null) {
                                            Bitmap bmp = BitmapFactory.decodeByteArray(data, 0, data.length);
                                            briquedodestinatario.setImageBitmap(bmp);
                                        }
                                    }
                                });
                            }

                            if (remetentebrinquedo != null) {
                                remetentebrinquedo.getDataInBackground(new GetDataCallback() {
                                    public void done(byte[] data, ParseException e) {
                                        if (e == null) {
                                            Bitmap bmp2 = BitmapFactory.decodeByteArray(data, 0, data.length);
                                            briquedoremetente.setImageBitmap(bmp2);
                                        } else {
                                            //  Log.d("BUZZ", "BUZZ");
                                        }
                                    }
                                });
                            }

                            destinatario.setText(destinatarionome);
                            remetente.setText(remetentenome);
                            edt_mensagem.setText(mensagem);
                            aceito.setVisibility(View.VISIBLE);
                            aceitar.setVisibility(View.VISIBLE);

                            ActivitySugestaoTroca.remetentenome = destinatarionome;
                            ActivitySugestaoTroca.destinatarionome = remetentenome;
                            ActivitySugestaoTroca.destinatariobrinquedo = remetentebrinquedo;
                            ActivitySugestaoTroca.remetentebrinquedo = destinatariobrinquedo;

                            ActivityFinalizarTroca.remetentenome = destinatarionome;
                            ActivityFinalizarTroca.destinatarionome = remetentenome;


                        } else {
                            pdialog.dismiss();
                            Log.i("SCRIPT_OBJID", e.getMessage());
                        }


                    }
                });
            } catch (JSONException e) {
                pdialog.dismiss();
                Log.i("SCRIPT_OBJID", e.getMessage());
                e.printStackTrace();
            }
        }
    }

    public void sendParse() {
        try {

            ParseQuery pushQuery = ParseInstallation.getQuery();
            pushQuery.whereEqualTo("username", remetentenome);
            ParsePush push = new ParsePush();
            push.setQuery(pushQuery);
            push.setMessage("{" + "\"msg\":" + "\"Novas mensagem no seu BrinquedoTroca\"" +
                    ",\"id\":" + objectIdAceitarTroca +
                    //",\"ob\":" + "\"aceitartroca\"" +
                    ",\"ob\":" + "\"aceitartroca\"" + "}");
            push.sendInBackground();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void alertDisplayer() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityAceitarTroca.this)
                .setTitle("Problemas com o servidor ")
                .setMessage("Desculpe ... Redirecionando você para o login ")
                //   .setIcon(R.drawable.ic_launcher)
                .setNegativeButton("NÂO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Intent intent = new Intent(ActivityAceitarTroca.this, ActivityLogout.class);
                        startActivity(intent);
                        ParseUser.logOut();

                    }
                })
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Intent intent = new Intent(ActivityAceitarTroca.this, ActivityLogin.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                });
        builder.create().show();
    }

    private void alertDisplayeReceberNovasMensagens() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityAceitarTroca.this)
                //  .setTitle(remetentenome + " , você deseja ... ")
                .setMessage("Enviando confirmação de troca para " + " " + remetentenome)
                //   .setIcon(R.drawable.ic_launcher)
                .setPositiveButton(" OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Intent intent = new Intent(context, ActivityHome.class);
                        startActivity(intent);

                    }
                });
        builder.create().show();
    }

}
