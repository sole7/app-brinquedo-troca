package com.aplicativo.sole.brinquedotroca;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import java.util.List;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class ActivityEmail extends AppCompatActivity implements OnClickListener {

    Session session = null;
    ProgressDialog pdialog = null;
    Context context = null;
    EditText msg;
    String rec, subject, textMessage, assunto;
    public static String emailuserlogado = null;
    public static String nomeuserlogado = null;


    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_email);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        context = this;
        ImageButton login = (ImageButton) findViewById(R.id.btn_submit);
        assunto = " Informar  um problema ";

        msg = (EditText) findViewById(R.id.ed_text);
        msg.getText().toString();

        login.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        subject = assunto;
        rec = "brinquedotroca10@gmail.com";
        textMessage = msg.getText().toString() + " " + emailuserlogado + " " + nomeuserlogado;

        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

        session = Session.getDefaultInstance(props, new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("brinquedotroca10@gmail.com", "101ou102");
            }
        });

        pdialog = ProgressDialog.show(context, "", "Enviando Mensagem...", true);
        RetreiveFeedTask task = new RetreiveFeedTask();
        task.execute();

    }

    class RetreiveFeedTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {

            try {

                Message message = new MimeMessage(session);
                message.setFrom(new InternetAddress("testfrom354@gmail.com"));
                message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(rec));
                message.setSubject(subject);
                message.setContent(textMessage, "text/html; charset=utf-8");
                Transport.send(message);
            } catch (MessagingException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            pdialog.dismiss();
            Toast.makeText(getApplicationContext(), " Mensagem  enviada !! ", Toast.LENGTH_LONG).show();
            alertDisplayer();

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_email, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.mensagem) {

            Intent intent = new Intent(context, ActivityVisualizarMensagem.class);
            startActivity(intent);

        } else if (id == R.id.carregar) {

            Intent intent = new Intent(context, ActivityCarregarBrinquedo.class);
            startActivity(intent);


        } else if (id == R.id.visualizar) {

            Intent intent = new Intent(context, ActivityVisualizarBrinquedo.class);
            startActivity(intent);


        } else if (id == R.id.sair) {

            Intent intent = new Intent(context, ActivityLogout.class);
            startActivity(intent);


        } else {
            Intent intent = new Intent(context, ActivityHome.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);

    }


    private void alertDisplayer() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityEmail.this)
                .setMessage(" Obrigado" + " " + nomeuserlogado + ", " + " entraremos em contato  no seu endereço de email: " + emailuserlogado)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Intent intent = new Intent(context, ActivityHome.class);
                        startActivity(intent);
                        dialog.cancel();
                    }
                });
        builder.create().show();
        msg.setText(" ");
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

}
