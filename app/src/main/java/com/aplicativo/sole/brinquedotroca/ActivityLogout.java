package com.aplicativo.sole.brinquedotroca;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.facebook.login.Login;
import com.facebook.login.LoginManager;
import com.parse.Parse;
import com.parse.ParseFacebookUtils;
import com.parse.ParseUser;

import static com.aplicativo.sole.brinquedotroca.ActivityLogin.LOGADO_PREF;

public class ActivityLogout extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Parse.initialize(ActivityLogout.this);
        setContentView(R.layout.layout_logout);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        alertDisplayer();
    }

    private void alertDisplayer() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityLogout.this)
                .setTitle(" Fechar aplicativo")
                .setMessage(" Ok !! Até logo ...")
                .setNegativeButton(" Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Intent intent = new Intent(ActivityLogout.this, ActivityHome.class);
                        startActivity(intent);
                        ParseUser.logOut();

                    }
                })
                .setPositiveButton(" OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();

                        SharedPreferences myPrefs = getSharedPreferences(LOGADO_PREF, Context.MODE_PRIVATE);
                        SharedPreferences.Editor prefsEditor = myPrefs.edit();
                        prefsEditor.putBoolean("estalogado", false);
                        prefsEditor.commit();

                        Intent intent = new Intent(ActivityLogout.this, ActivityLogin.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);


                        finish();
                    }
                });
        builder.create().show();
    }
}



