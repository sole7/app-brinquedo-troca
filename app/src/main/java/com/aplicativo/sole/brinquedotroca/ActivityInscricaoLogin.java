package com.aplicativo.sole.brinquedotroca;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

public class ActivityInscricaoLogin extends AppCompatActivity {

    private EditText usernameView;
    private EditText passwordView;
    private EditText emailView;
    private EditText passwordAgainView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Parse.initialize(this);
        setContentView(R.layout.layout_inscricaologin);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActivityInscricaoLogin.this, ActivityLogin.class);
                startActivity(intent);
            }
        });

        usernameView = (EditText) findViewById(R.id.username);
        emailView = (EditText) findViewById(R.id.email);
        passwordView = (EditText) findViewById(R.id.password);
        passwordAgainView = (EditText) findViewById(R.id.passwordAgain);

        final Button signup_button = findViewById(R.id.signup_button);
        signup_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Validating the log in data
                boolean validationError = false;

                StringBuilder validationErrorMessage = new StringBuilder("Por favor, insira ");
                if (isEmpty(usernameView)) {
                    validationError = true;
                    validationErrorMessage.append("o seu nome de usuário");
                }
                if (isEmpty(emailView) || !isEmailValid(emailView.getText().toString())) {
                    if (validationError) {
                        validationErrorMessage.append(" e ");
                    }
                    validationError = true;
                    validationErrorMessage.append("um endereço de email válido");
                }
                if (isEmpty(passwordView) || !isPasswordValid(passwordView.getText().toString())) {
                    if (validationError) {
                        validationErrorMessage.append(" e ");
                    }
                    validationError = true;
                    validationErrorMessage.append("uma senha válida");
                }
                if (isEmpty(passwordAgainView)) {
                    if (validationError) {
                        validationErrorMessage.append(" e ");
                    }
                    validationError = true;
                    validationErrorMessage.append("sua senha novamente");
                } else {
                    if (!isMatching(passwordView, passwordAgainView)) {
                        if (validationError) {
                            validationErrorMessage.append(" e ");
                        }
                        validationError = true;
                        validationErrorMessage.append("a mesma senha duas vezes.");
                    }
                }
                validationErrorMessage.append(".");

                if (validationError) {
                    Toast.makeText(ActivityInscricaoLogin.this, validationErrorMessage.toString(), Toast.LENGTH_LONG).show();
                    return;
                }

                //Setting up a progress dialog
                final ProgressDialog dlg = new ProgressDialog(ActivityInscricaoLogin.this);
                dlg.setTitle(" Inscrever-se ");
                dlg.setMessage(" Por favor, aguarde ...");
                //     dlg.setIcon(R.drawable.ic_launcher);
                dlg.show();

                try {
                    // Reset errors.
                    emailView.setError(null);
                    passwordView.setError(null);

                    ParseUser user = new ParseUser();
                    user.setUsername(usernameView.getText().toString());
                    user.setPassword(passwordView.getText().toString());
                    user.setEmail(emailView.getText().toString());

                    user.signUpInBackground(new SignUpCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {
                                dlg.dismiss();
                                ParseUser.logOut();
                                alertDisplayer(" Conta criada !", " Por favor, verifique seu email antes do Login.", false);
                            } else {
                                dlg.dismiss();
                                ParseUser.logOut();
                                alertDisplayer(" Conta falhou !", "Desculpe, credenciais usadas por outro usuário ", true);
                            }
                        }
                    });
                } catch (Exception e) {
                    dlg.dismiss();
                    e.printStackTrace();
                }
            }
        });
    }

    private boolean isEmpty(EditText text) {
        if (text.getText().toString().trim().length() > 0) {
            return false;
        } else {
            return true;
        }
    }

    private boolean isMatching(EditText text1, EditText text2) {
        if (text1.getText().toString().equals(text2.getText().toString())) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isEmailValid(String email) {
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }

    private void alertDisplayer(String title, String message, final boolean error) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityInscricaoLogin.this)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(" OK ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        if (!error) {
                            Intent intent = new Intent(ActivityInscricaoLogin.this, ActivityInscricaoLogin.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                    }
                });
        AlertDialog ok = builder.create();
        ok.show();
    }

}
