package com.aplicativo.sole.brinquedotroca;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;

import java.util.ArrayList;

public class AdapterListViewVisualizarBrinquedo extends BaseAdapter {

    private LayoutInflater mInflater = null;
    private ArrayList<ParseObject> itens = null;
    private boolean mChecked = false;

    public AdapterListViewVisualizarBrinquedo(Context context, ArrayList<ParseObject> itens) {
        this.itens = itens;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return itens.size();
    }

    @Override
    public Object getItem(int i) {
        return itens.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        ItemSuporte itemHolder;

        if (view == null) {
            view = mInflater.inflate(R.layout.itemlist_visualizarbrinquedo, null);
            itemHolder = new ItemSuporte();
            itemHolder.txtTitle = ((TextView) view.findViewById(R.id.description));
            itemHolder.imgIcon = ((ImageView) view.findViewById(R.id.ivphoto));
            view.setTag(itemHolder);

        } else {
            itemHolder = (ItemSuporte) view.getTag();
        }

        if (itens.size() > 0) {
            ParseObject item = itens.get(i);
            ParseFile im = (ParseFile) item.get("image");
            // itemHolder.txtTitle.setText(itemlist_visualizarbrinquedo.get("descricao").toString());

            try {
                byte[] b = im.getData();
                Bitmap bitmap = this.decodeSampledBitmapFromResource(b, R.id.ivphoto, 100, 100);
                itemHolder.imgIcon.setImageBitmap(bitmap);
            } catch (ParseException sEx) {


            }
        }

        return view;
    }

    private Bitmap decodeSampledBitmapFromResource(byte[] data, int resId, int reqWidth, int reqHeight) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(data, 0, data.length, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeByteArray(data, 0, data.length, options);
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }

        return inSampleSize;
    }

    private class ItemSuporte {
        ImageView imgIcon;
        TextView txtTitle;
    }

    public void remove(int position) {
        itens.remove(position);
        this.notifyDataSetChanged();
    }

    public void limpar() {

        ArrayList<ParseObject> itens = null;
        itens.get(0);
        this.notifyDataSetChanged();
    }

}
