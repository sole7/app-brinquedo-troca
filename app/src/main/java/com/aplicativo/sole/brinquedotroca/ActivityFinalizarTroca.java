package com.aplicativo.sole.brinquedotroca;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseFile;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import tgio.parselivequery.BaseQuery;
import tgio.parselivequery.LiveQueryClient;
import tgio.parselivequery.LiveQueryEvent;
import tgio.parselivequery.Subscription;
import tgio.parselivequery.interfaces.OnListener;

public class ActivityFinalizarTroca extends AppCompatActivity {


    public static String destinatarionome = null;
    public static String remetentenome = null;
    public static String icon = null;
    final Context context = this;
    ProgressDialog pdialog = null;
    public static String objectIdFinalizarTroca = null;
    public static boolean naosouresposta = true;
    public String texto;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Parse.initialize(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_finalizartroca);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        final EditText message = (EditText) findViewById(R.id.pokeText);

        final ImageButton geolocalizacao = (ImageButton) findViewById(R.id.btn_geolocalizacao);
        geolocalizacao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ActivityMapa.class);
                startActivity(intent);
            }
        });

        Intent it = getIntent();
        JSONObject jobj = null;

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                pdialog = ProgressDialog.show(context, " Enviando mensagem ", "Por favor, aguarde ...", true);
                if (message.getText().equals(" ")) {
                    pdialog.dismiss();
                    Toast.makeText(getApplicationContext(), " Ops !! Você não digitou uma mensagem !", Toast.LENGTH_LONG).show();
                } else {

                    final ParseObject poke = new ParseObject("FinalizarTroca");
                    if (!naosouresposta) {
                        poke.put("destinatario", remetentenome);
                        poke.put("remetente", destinatarionome);
                    } else {
                        poke.put("destinatario", destinatarionome);
                        poke.put("remetente", remetentenome);
                    }

                    poke.put("type", message.getText().toString());
                    poke.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            objectIdFinalizarTroca = poke.getObjectId();
                            pdialog.dismiss();
                            Snackbar.make(view, "Mensagem enviada!", Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                            sendParse();
                            message.setText(" ");
                            texto = poke.get("type").toString();
                            alertDisplayeFinalizarMensagens();
                        }
                    });
                }
            }
        });


        try {

            pdialog = ProgressDialog.show(context, " Notificação ", " Por favor, aguarde ...", true);

            jobj = new JSONObject(it.getExtras().getString("com.parse.Data"));
            JSONObject objTroca = new JSONObject(jobj.get("alert").toString());
            final String id = objTroca.get("id").toString();

            ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("AceitarTroca");
            query.whereEqualTo("objectId", id);
            query.getFirstInBackground(new GetCallback<ParseObject>() {
                @Override
                public void done(ParseObject status, ParseException e) {
                    if (e == null) {
                        pdialog.dismiss();
                        destinatarionome = status.get("remetente").toString();
                        remetentenome = status.get("destinatario").toString();
                        alertDisplayerEnviarMensagem();
                    } else {
                        pdialog.dismiss();
                        Log.i("SCRIPT_OBJID", e.getMessage());
                    }
                }
            });

            ParseQuery<ParseObject> query2 = new ParseQuery<ParseObject>("FinalizarTroca");
            query2.whereEqualTo("objectId", id);
            query2.getFirstInBackground(new GetCallback<ParseObject>() {
                @Override
                public void done(ParseObject status, ParseException e) {
                    if (e == null) {
                        pdialog.dismiss();
                        remetentenome = status.get("remetente").toString();
                        destinatarionome = status.get("destinatario").toString();
                        String msg = status.get("type").toString();

                        alertDisplayeResponderMensagens();
                        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.liner_destinatario);
                        linearLayout.setVisibility(View.VISIBLE);

                        TextView nomedestinatario = (TextView) findViewById(R.id.tx_nomedestinatario);
                        nomedestinatario.setText(remetentenome);

                        TextView msgdestinatario = (TextView) findViewById(R.id.tx_mensagem);
                        msgdestinatario.setText(msg);

                    } else {
                        pdialog.dismiss();
                        Log.i("SCRIPT_OBJID", e.getMessage());
                        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.liner_destinatario);
                        linearLayout.setVisibility(View.INVISIBLE);

                    }
                }
            });

        } catch (JSONException e) {
            pdialog.dismiss();
            alertDisplayer();
            Log.i("SCRIPT_OBJID", e.getMessage());
            e.printStackTrace();
        }
    }

    int numPokes = 0;

    private void alertDisplayer() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityFinalizarTroca.this)
                .setTitle(" Problemas com o servidor ")
                .setMessage("Desculpe ... Redirecionando você para o login ")
                //   .setIcon(R.drawable.ic_launcher)
                .setNegativeButton(" NÃO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Intent intent = new Intent(ActivityFinalizarTroca.this, ActivityLogout.class);
                        startActivity(intent);
                        ParseUser.logOut();

                    }
                })
                .setPositiveButton(" OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Intent intent = new Intent(ActivityFinalizarTroca.this, ActivityLogin.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                });
        builder.create().show();
    }

    private void alertDisplayerEnviarMensagem() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityFinalizarTroca.this)
                .setTitle(" Sugestão de Troca aceita !!! ")
                .setMessage("Escrever mensagem para " + "" + destinatarionome + " ?")
                //   .setIcon(R.drawable.ic_launcher)
                .setNegativeButton(" NÃO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Intent intent = new Intent(context, ActivityHome.class);
                        startActivity(intent);
                    }
                })
                .setPositiveButton(" SIM", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        final TextView messageBoard = (TextView) findViewById(R.id.message);
                        messageBoard.setMovementMethod(new ScrollingMovementMethod());
                        messageBoard.setVisibility(View.VISIBLE);

                        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
                        fab.setVisibility(View.VISIBLE);

                        final EditText message = (EditText) findViewById(R.id.pokeText);
                        message.setVisibility(View.VISIBLE);
                        dialog.cancel();

                    }
                });
        builder.create().show();
    }


    private void alertDisplayeResponderMensagens() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityFinalizarTroca.this)
                //  .setTitle(remetentenome + " , você deseja ... ")
                .setMessage("Responder mensagens de " + remetentenome + " ?")
                //   .setIcon(R.drawable.ic_launcher)
                .setNegativeButton(" NÃO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Intent intent = new Intent(context, ActivityHome.class);
                        startActivity(intent);
                    }
                })
                .setPositiveButton("SIM", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        naosouresposta = false;

                        final TextView messageBoard = (TextView) findViewById(R.id.message);
                        messageBoard.setMovementMethod(new ScrollingMovementMethod());
                        messageBoard.setVisibility(View.VISIBLE);

                        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
                        fab.setVisibility(View.VISIBLE);

                        final EditText message = (EditText) findViewById(R.id.pokeText);
                        message.setVisibility(View.VISIBLE);


                    }
                });
        builder.create().show();
    }

    public void sendParse() {

        try {
            ParseQuery pushQuery = ParseInstallation.getQuery();
            if (!naosouresposta) {
                pushQuery.whereEqualTo("username", remetentenome);
            } else {
                pushQuery.whereEqualTo("username", destinatarionome);
            }

            ParsePush push = new ParsePush();
            push.setQuery(pushQuery);
            push.setMessage("{" + "\"msg\":" + "\"Novas mensagem no seu BrinquedoTroca\"".toString() +
                    ",\"id\":" + objectIdFinalizarTroca.toString() +
                    ",\"ob\":" + "\"aceitartroca\"".toString() +
                    "}");
            push.sendInBackground();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void alertDisplayeFinalizarMensagens() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityFinalizarTroca.this)
                //  .setTitle(remetentenome + " , você deseja ... ")
                .setMessage(" Mensagem enviada ! ")
                //   .setIcon(R.drawable.ic_launcher)
                .setPositiveButton(" OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Intent intent = new Intent(context, ActivityHome.class);
                        startActivity(intent);

                    }
                });
        builder.create().show();
    }

}

