package com.aplicativo.sole.brinquedotroca;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseFile;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SendCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import tgio.parselivequery.BaseQuery;
import tgio.parselivequery.LiveQueryClient;
import tgio.parselivequery.LiveQueryEvent;
import tgio.parselivequery.Subscription;
import tgio.parselivequery.interfaces.OnListener;

public class ActivitySugestaoTroca extends AppCompatActivity {

    public static String destinatarionome = null;
    public static String destinatariodescricao = null;
    public static ParseFile destinatariobrinquedo = null;
    public static String remetentenome = null;
    public static ParseFile remetentebrinquedo = null;
    public static String objectId = null;

    public static String icon = null;
    final Context context = this;

    private ListView listView = null;
    public AdapterListViewSugestaoTroca adapterListView = null;
    private ArrayList<ParseObject> itens = null;

    private ListView listView2 = null;
    private ArrayList<ParseObject> itens2 = null;
    public AdapterListViewSugestaoTroca adapterListView2 = null;

    ProgressDialog pdialog = null;
    private static boolean isInitialized = false;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_sugestaotroca);

        final ImageView briquedodestinatario = (ImageView) findViewById(R.id.iv_brinquedodestinatario);
        final ImageView briquedoremetente = (ImageView) findViewById(R.id.iv_brinquedoremetente);

        final TextView destinatario = (TextView) findViewById(R.id.tv_destinatario);
        final TextView remetente = (TextView) findViewById(R.id.tv_trocas);

        final FrameLayout frameLayoutlista = (FrameLayout) findViewById(R.id.framelist);

        listView = (ListView) findViewById(R.id.list_mensagens);
        listView2 = (ListView) findViewById(R.id.list_mensagens2);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        pdialog = ProgressDialog.show(context, " ", " Por favor, aguarde ...", true);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        final TextView messageBoard = (TextView) findViewById(R.id.message);
        messageBoard.setMovementMethod(new ScrollingMovementMethod());

        final EditText message = (EditText) findViewById(R.id.pokeText);

        Parse.initialize(this);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                pdialog = ProgressDialog.show(context, " Enviando sugestao de  troca ", "Por favor, aguarde ...", true);

                ActivityAceitarTroca.enviada = false;

                if (briquedoremetente.getDrawable() == null) {
                    pdialog.dismiss();
                    Toast.makeText(getApplicationContext(), " Ops !! Você não selecionou seu brinquedo !", Toast.LENGTH_LONG).show();
                } else {

                    final ParseObject poke = new ParseObject("SugestaoTroca");
                    poke.put("destinatario", destinatarionome);
                    poke.put("remetente", remetentenome);
                    poke.put("foto", destinatariobrinquedo);
                    poke.put("fotoremetente", remetentebrinquedo);
                    poke.put("type", message.getText().toString());
                    poke.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            pdialog.dismiss();
                            objectId = poke.getObjectId();
                            destinatariodescricao = poke.get("type").toString();

                            Snackbar.make(view, "Sugestâo de troca enviada!", Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                            sendParse();
                            message.setText(" ");
                            alertDisplayerEnviarNovasTrocas();
                        }
                    });

                }
            }
        });

        ParseQuery<ParseObject> query = null;
        query = new ParseQuery<ParseObject>("Brinquedo");
        query.whereEqualTo("usuario", destinatarionome);
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> pobjs, ParseException e) {
                if (e == null) {
                    pdialog.dismiss();
                    itens = (ArrayList) pobjs;
                    adapterListView = new AdapterListViewSugestaoTroca(getApplicationContext(), itens);
                    listView.setAdapter(adapterListView);
                    listView.setCacheColorHint(Color.TRANSPARENT);

                } else {
                    pdialog.dismiss();
                    alertDisplayer();
                    // Toast.makeText(getApplicationContext(), "Desculpe, falha ao se conectar no servidor!!", Toast.LENGTH_LONG).show();
                    finish();
                }

            }
        });

        ParseQuery<ParseObject> query2 = null;
        query2 = new ParseQuery<ParseObject>("Brinquedo");
        query2.whereEqualTo("usuario", remetentenome);
        query2.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> pobjs, ParseException e) {
                if (e == null) {
                    pdialog.dismiss();
                    itens2 = (ArrayList) pobjs;
                    adapterListView2 = new AdapterListViewSugestaoTroca(getApplicationContext(), itens2);
                    listView2.setAdapter(adapterListView2);
                    listView2.setCacheColorHint(Color.TRANSPARENT);
                } else {
                    pdialog.dismiss();
                    alertDisplayer();
                    finish();
                }

            }
        });


        remetente.setText("Trocas selecionadas");
        destinatario.setText("Selecionar brinquedo de " + "" + destinatarionome + " " + "aqui !");

        destinatariobrinquedo.getDataInBackground(new GetDataCallback() {
            public void done(byte[] data, ParseException e) {
                if (e == null) {
                    Bitmap bmp = BitmapFactory.decodeByteArray(data, 0, data.length);
                    briquedodestinatario.setImageBitmap(bmp);
                }
            }
        });

        listView2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int i, long arg3) {

                remetente.setVisibility(View.VISIBLE);
                remetentebrinquedo = itens2.get(i).getParseFile("image");
                pdialog = ProgressDialog.show(context, "  ", "Por favor, aguarde ...", true);
                remetentebrinquedo.getDataInBackground(new GetDataCallback() {
                    public void done(byte[] data, ParseException e) {
                        if (e == null) {
                            pdialog.dismiss();
                            Bitmap bmp = BitmapFactory.decodeByteArray(data, 0, data.length);
                            ImageView briquedoremetente = (ImageView) findViewById(R.id.iv_brinquedoremetente);
                            briquedoremetente.setImageBitmap(bmp);
                        } else {
                            pdialog.dismiss();
                        }
                    }
                });
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int i, long arg3) {
                pdialog = ProgressDialog.show(context, "  ", "Por favor, aguarde ...", true);

                destinatariobrinquedo = itens.get(i).getParseFile("image");
                destinatariobrinquedo.getDataInBackground(new GetDataCallback() {
                    public void done(byte[] data, ParseException e) {
                        if (e == null) {
                            pdialog.dismiss();
                            Bitmap bmp = BitmapFactory.decodeByteArray(data, 0, data.length);
                            ImageView briquedodestinatario = (ImageView) findViewById(R.id.iv_brinquedodestinatario);
                            briquedodestinatario.setImageBitmap(bmp);
                            pdialog.dismiss();

                        }
                    }
                });
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_mensagem, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.mensagem) {

            Intent intent = new Intent(context, ActivityVisualizarMensagem.class);
            startActivity(intent);

        } else if (id == R.id.home) {

            Intent intent = new Intent(context, ActivityHome.class);
            startActivity(intent);

        } else if (id == R.id.carregar) {

            Intent intent = new Intent(context, ActivityCarregarBrinquedo.class);
            startActivity(intent);

        } else if (id == R.id.visualizar) {

            Intent intent = new Intent(context, ActivityVisualizarBrinquedo.class);
            startActivity(intent);

        } else if (id == R.id.sair) {
            Intent intent = new Intent(context, ActivityLogout.class);
            startActivity(intent);

        } else {

            Intent intent = new Intent(context, ActivityEmail.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);

    }

    int numPokes = 0;

    private void alertDisplayer() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ActivitySugestaoTroca.this)
                .setTitle(" Problemas com o servidor ")
                .setMessage("Desculpe ... Por favor, logar novamente ")
                //   .setIcon(R.drawable.ic_launcher)
                .setNegativeButton(" NÃO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Intent intent = new Intent(ActivitySugestaoTroca.this, ActivityLogout.class);
                        startActivity(intent);
                        ParseUser.logOut();

                    }
                })
                .setPositiveButton(" OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Intent intent = new Intent(ActivitySugestaoTroca.this, ActivityLogin.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                });
        builder.create().show();
    }


    private void alertDisplayerEnviarNovasTrocas() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ActivitySugestaoTroca.this)
                //  .setTitle(remetentenome + " , você deseja ... ")
                .setMessage("Sugerir novas trocas  para " + " " + destinatarionome)
                //   .setIcon(R.drawable.ic_launcher)
                .setNegativeButton(" NÃO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Intent intent = new Intent(context, ActivityHome.class);
                        startActivity(intent);
                    }
                })
                .setPositiveButton(" SIM", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();

                    }
                });
        builder.create().show();
    }


    public void sendParse() {
        try {
            ParseQuery pushQuery = ParseInstallation.getQuery();
            pushQuery.whereEqualTo("username", destinatarionome);
            ParsePush push = new ParsePush();
            push.setQuery(pushQuery);
        /*    JSONObject data = new JSONObject("{" + "\"msg\":" + "\"Novas solicitações de mensagem no seu BrinquedoTroca\"" +
                    ",\"id\":" + objectId.toString +
                    ",\"ob\":" + "\"sugestaotroca\"".toString() +"}"); */
            push.setMessage("{" + "\"msg\":" + "\"Novas mensagem no seu BrinquedoTroca\"" +
                    ",\"id\":" + objectId +
                    ",\"ob\":" + "\"sugestaotroca\"" + "}");
            //    push.setData(data);
            push.sendInBackground();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

