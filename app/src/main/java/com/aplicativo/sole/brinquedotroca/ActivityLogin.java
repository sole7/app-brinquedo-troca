package com.aplicativo.sole.brinquedotroca;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseUser;
import com.parse.ParseInstallation;
import com.parse.SaveCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Collection;

public class ActivityLogin extends AppCompatActivity {

    AccessToken accessToken;
    CallbackManager callbackManager;
    Button login;
    Button loginface;
    EditText name = null;
    EditText senha = null;
    final Context context = this;
    TextView primeiroacesso;
    String emaildonobrinquedo;
    public static final String LOGADO_PREF = "estalogado";
    private static boolean isParseFacebookInitialized = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Parse.initialize(this);

        if (!isParseFacebookInitialized) {
            ParseFacebookUtils.initialize(this);
            isParseFacebookInitialized = true;
        }

        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_login);

        login = (Button) findViewById(R.id.btnlogin);
        loginface = (Button) findViewById(R.id.login_button);

        name = (EditText) findViewById(R.id.txtname);
        senha = (EditText) findViewById(R.id.txtsenha);

        primeiroacesso = (TextView) findViewById(R.id.txtacesso);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        SharedPreferences myPrefs = getSharedPreferences(LOGADO_PREF, Context.MODE_PRIVATE);
        boolean jaLogou = myPrefs.getBoolean("estalogado", false);

        if (jaLogou) {
            ActivityCarregarBrinquedo.user = myPrefs.getString("user", "");
            ActivityCarregarBrinquedo.emaildonobrinquedo = myPrefs.getString("email", "");
            ActivityEmail.nomeuserlogado = myPrefs.getString("user", "");
            ActivityEmail.emailuserlogado = myPrefs.getString("user", "");
            ActivityVisualizarBrinquedo.nomeluserlogado = myPrefs.getString("user", "");
            ActivityVisualizarMensagem.nomeluserlogado = myPrefs.getString("user", "");
            ActivityAceitarTroca.nomeluserlogado = myPrefs.getString("user", "");
            ActivityEmail.emailuserlogado = myPrefs.getString("email", "");
            Intent intent = new Intent(context, ActivityHome.class);
            startActivity(intent);
        }

        primeiroacesso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final ProgressDialog dlg = new ProgressDialog(ActivityLogin.this);
                dlg.setTitle(" Por favor, aguarde um momento !");
                dlg.setMessage(" Redirecionando você para se registrar ...");
                dlg.show();
                Intent intent = new Intent(ActivityLogin.this, ActivityInscricaoLogin.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                dlg.dismiss();
                startActivity(intent);

            }

        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean validationError = false;
                StringBuilder validationErrorMessage = new StringBuilder("Por favor, insira ");
                if (isEmpty(name)) {
                    validationError = true;
                    validationErrorMessage.append("o seu nome de usuário");
                }
                if (isEmpty(senha) || !isPasswordValid(senha.getText().toString())) {
                    if (validationError) {
                        validationErrorMessage.append(" e ");
                    }
                    validationError = true;
                    validationErrorMessage.append(" uma senha valida e maior que 02 caracteres ");
                }
                validationErrorMessage.append(".");

                if (validationError) {
                    Toast.makeText(ActivityLogin.this, validationErrorMessage.toString(), Toast.LENGTH_LONG).show();
                    return;
                }

                ParseInstallation installation = ParseInstallation.getCurrentInstallation();
                installation.put("GCMSenderId", "909839541153");
                installation.put("username", name.getText().toString());
                installation.saveInBackground();

                SharedPreferences myPrefs = getSharedPreferences(LOGADO_PREF, Context.MODE_PRIVATE);
                final SharedPreferences.Editor prefsEditor = myPrefs.edit();

                final ProgressDialog dlg = new ProgressDialog(ActivityLogin.this);
                dlg.setTitle(" Autenticando Login");
                dlg.setMessage(" Por favor, aguarde ...");
                dlg.show();

                senha.setError(null);


                ParseUser.logInInBackground(name.getText().toString(), senha.getText().toString(), new LogInCallback() {
                    @Override
                    public void done(ParseUser parseUser, ParseException e) {
                        if (parseUser != null) {
                            if (parseUser.getBoolean("emailVerified")) {
                                dlg.dismiss();
                                // alertDisplayer("Login realizado com sucesso! ", "Bem vindo, " + parseUser.getUsername().toString() + "!", false);
                                parseUser.getEmail();
                                emaildonobrinquedo = parseUser.getEmail();
                                prefsEditor.putBoolean("estalogado", true);
                                prefsEditor.putString("user", name.getText().toString());
                                prefsEditor.putString("email", emaildonobrinquedo);
                                prefsEditor.apply();

                                Toast.makeText(getApplicationContext(), " Bem vindo " + " " + parseUser.getUsername() + " !!", Toast.LENGTH_LONG).show();

                                Intent intent = new Intent(context, ActivityHome.class);
                                ActivityCarregarBrinquedo.user = name.getText().toString();
                                ActivityEmail.nomeuserlogado = name.getText().toString();
                                ActivityVisualizarBrinquedo.nomeluserlogado = name.getText().toString();
                                ActivityVisualizarMensagem.nomeluserlogado = name.getText().toString();
                                ActivityAceitarTroca.nomeluserlogado = name.getText().toString();
                                ActivityEmail.emailuserlogado = emaildonobrinquedo;
                                ActivityCarregarBrinquedo.emaildonobrinquedo = emaildonobrinquedo;
                                startActivity(intent);
                            } else {
                                ParseUser.logOut();
                                dlg.dismiss();
                                alertDisplayer(" Falha no Login ", " Por favor, verifique seu e-mail primeiro", true);
                            }
                        } else {
                            ParseUser.logOut();
                            dlg.dismiss();
                            alertDisplayer(" Falha no Login", " Por favor, tente novamente ", true);
                        }
                    }
                });
            }
        });

        loginface.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final ProgressDialog dlg = new ProgressDialog(ActivityLogin.this);
                //  dlg.setTitle(" Abrindo navegador");
                dlg.setMessage(" Por favor, aguarde ...");
                dlg.show();

                Collection<String> permissions = Arrays.asList("public_profile", "email");
                ParseFacebookUtils.logInWithReadPermissionsInBackground(ActivityLogin.this, permissions, new LogInCallback() {

                    @Override
                    public void done(ParseUser user, ParseException err) {
                        if (err != null) {
                            dlg.dismiss();
                            ParseUser.logOut();
                            Log.e("err", "err", err);
                        }
                        if (user == null) {
                            dlg.dismiss();
                            ParseUser.logOut();
                            Toast.makeText(ActivityLogin.this, "Desculpe, falha ao entrar pelo Facebook", Toast.LENGTH_LONG).show();
                            Log.d("MyApp", "Uh oh. The user cancelled the Facebook login.");
                        } else if (user.isNew()) {
                            dlg.dismiss();
                            //   Toast.makeText(ActivityLogin.this, "Autenticado pelo Facebook", Toast.LENGTH_LONG).show();
                            Log.d("MyApp", "User signed up and logged in through Facebook!");
                            getUserDetailFromFB();
                        } else {
                            dlg.dismiss();

                            ParseInstallation installation = ParseInstallation.getCurrentInstallation();
                            installation.put("GCMSenderId", "909839541153");
                            installation.put("username", user.getUsername());
                            installation.saveInBackground();

                            SharedPreferences myPrefs = getSharedPreferences(LOGADO_PREF, Context.MODE_PRIVATE);
                            final SharedPreferences.Editor prefsEditor = myPrefs.edit();

                            prefsEditor.putBoolean("estalogado", true);
                            prefsEditor.putString("user", user.getUsername());
                            prefsEditor.putString("email", user.getEmail());
                            prefsEditor.apply();

                            ActivityCarregarBrinquedo.user = user.getUsername();
                            ActivityEmail.nomeuserlogado = user.getUsername();
                            ActivityEmail.emailuserlogado = user.getEmail();
                            ActivityVisualizarBrinquedo.nomeluserlogado = user.getUsername();
                            ActivityVisualizarMensagem.nomeluserlogado = user.getUsername();
                            //     Toast.makeText(ActivityLogin.this, "Autenticado pelo Facebook.", Toast.LENGTH_LONG).show();
                            Log.d("MyApp", "User logged in through Facebook!");
                            alertDisplayer(" ", "Bem vindo de volta!");

                        }
                    }
                });
            }
        });
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() >= 2;
    }

    private boolean isEmpty(EditText text) {
        return text.getText().toString().trim().length() <= 0;
    }

    private void alertDisplayer(String title, String message, final boolean error) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(ActivityLogin.this)
                // .setTitle(title)
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        if (!error) {
                            Intent intent = new Intent(ActivityLogin.this, ActivityLogin.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                    }
                });
        android.app.AlertDialog ok = builder.create();
        ok.show();
    }


    private void alertDisplayer(String title, String message) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(ActivityLogin.this)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Intent intent = new Intent(ActivityLogin.this, ActivityHome.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                });
        android.app.AlertDialog ok = builder.create();
        ok.show();
    }

    private void getUserDetailFromFB() {

        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(final JSONObject object, GraphResponse response) {

                final ParseUser user = ParseUser.getCurrentUser();
                try {

                    user.setUsername(object.getString("name"));


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    user.setEmail(object.getString("email"));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                user.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {

                        alertDisplayer("Login pela primeira vez!", "Bem vindo !!");

                    }
                });
            }

        });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "name,email");
        request.setParameters(parameters);
        request.executeAsync();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ParseFacebookUtils.onActivityResult(requestCode, resultCode, data);

    }
}


