package com.aplicativo.sole.brinquedotroca;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import de.greenrobot.event.EventBus;

import com.parse.ParsePushBroadcastReceiver;

import org.json.JSONException;
import org.json.JSONObject;


public class CustomPushReceiver extends ParsePushBroadcastReceiver {

    @Override
    protected int getSmallIconId(Context context, Intent intent) {
        return (R.drawable.logo10);
    }

    @Override
    protected void onPushOpen(Context c, Intent i) {

        JSONObject jobj = null;
        try {
            jobj = new JSONObject(i.getExtras().getString("com.parse.Data"));
            JSONObject objTroca = new JSONObject(jobj.get("alert").toString());

            if (objTroca.getString("ob").equals("sugestaotroca")) {
                Intent intent = new Intent(c, ActivityAceitarTroca.class);
                intent.putExtras(i.getExtras());
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                c.startActivity(intent);
                ActivityAceitarTroca.enviada = false;
            } else {
                Intent intent = new Intent(c, ActivityFinalizarTroca.class);
                intent.putExtras(i.getExtras());
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                c.startActivity(intent);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPushReceive(Context context, Intent intent) {
        if (intent == null) return;
        super.onPushReceive(context, intent);
    }

}
